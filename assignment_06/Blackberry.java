package cs102.week08;

public class Blackberry extends GroundFruit {
    public Blackberry() {
        this.color = "black";
    }

    public String getVitamin() {
        return "C K";
    }

    public void pick() {
        System.out.println("Picking a blackberry.");
    }
}
