package cs102.week08;

public abstract class GroundFruit extends Fruit {
    public abstract void pick();
}

