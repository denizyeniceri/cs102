package cs102.week08;

public abstract class TreeFruit extends Fruit {
    public abstract void peel();
}
