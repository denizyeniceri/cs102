package cs102.week14;

import javax.swing.*;
import java.awt.*;

public class MineSweeperGUI extends JPanel {
    private MineGrid grid;
    private JButton[][] buttons;

    public MineSweeperGUI(int numRows, int numCols, int numMines) {
        buttons = new JButton[numRows][numCols];
        grid = new MineGrid(numRows, numCols, numMines);

        setLayout(new GridLayout(numRows, numCols));
        for (int i = 0; i < numRows; ++i) {
            for (int j = 0; j < numCols; ++j) {
                ButtonHandler buttonHandler = new ButtonHandler(i, j, grid, buttons);
                buttons[i][j] = new JButton();
                add(buttons[i][j]);
                buttons[i][j].addActionListener(buttonHandler);
                buttons[i][j].addMouseListener(buttonHandler);

            }
        }
    }
}
