package cs102.week14;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

class ButtonHandler extends MouseAdapter implements ActionListener {
    private int row, col;
    private MineGrid grid;
    private JButton[][] buttons;

    public ButtonHandler(int x, int y, MineGrid g, JButton[][] buttons) {
        row = x;
        col = y;
        grid = g;
        this.buttons = buttons;
    }

    public void actionPerformed(ActionEvent event) {
        if (grid.isMINE(row, col)) {
            for (int i = 0; i < grid.sizeMineSweeper(); ++i) {
                for (int j = 0; j < grid.sizeMineSweeper(); ++j) {
                    if (grid.isMINE(i, j)) {
                        openBox(i, j);
                    }
                }
            }
            JOptionPane.showMessageDialog(null, "OOOPS!!");
            System.exit(0);
        } else {
            if (event.getSource() instanceof JButton) {
                openSides(row, col);
            }
        }
    }

    public void openSides(int i, int j) {
        if (grid.getCellContent(i, j) == 0) {
            openBox(i, j);
            // OpenRightSide
            if (grid.isInsideGrid(i, j + 1) && grid.isOpened(i, j + 1)) {
                openBox(i, j + 1);
                openSides(i, j + 1);
            }
            // openLeftSide
            if (grid.isInsideGrid(i, j - 1) && grid.isOpened(i, j - 1)) {
                openBox(i, j - 1);
                openSides(i, j - 1);
            }
            // openDownSide
            if (grid.isInsideGrid(i + 1, j) && grid.isOpened(i + 1, j)) {
                openBox(i + 1, j);
                openSides(i + 1, j);
            }
            // openUpSide
            if (grid.isInsideGrid(i - 1, j) && grid.isOpened(i - 1, j)) {
                openBox(i - 1, j);
                openSides(i - 1, j);
            }
        } else if (grid.getCellContent(i, j) > 0) {
            openBox(i, j);
        }
    }

    private void openBox(int i, int j) {
        JButton button = buttons[i][j];
        if (grid.getCellContent(i, j) == 0) {
            button.setText("");
            grid.openChange(i, j);
            button.setEnabled(false);
        } else if (grid.getCellContent(i, j) > 0) {
            button.setText(String.valueOf(grid.getCellContent(i, j)));
            grid.openChange(i, j);
            button.setEnabled(false);
        } else if (grid.getCellContent(i, j) == -1) {
            button.setText("M");
            grid.openChange(i, j);
            button.setBackground(Color.ORANGE);
        }

    }

    public void mousePressed(MouseEvent event) {

        if (event.getButton() == MouseEvent.BUTTON3) {
            JButton button = (JButton) event.getSource();
            if (button.getText().equals("F")) {
                button.setText("");
                button.setEnabled(true);
                button.setBackground(null);
                if (checkWinCon()) {
                    JOptionPane.showMessageDialog(null, "YOU WIN!!!");
                    System.exit(0);
                }
            } else if (button.getText().equals("")) {
                button.setText("F");
                button.setEnabled(false);
                button.setBackground(Color.RED);
                if (checkWinCon()) {
                    JOptionPane.showMessageDialog(null, "YOU WIN!!!");
                    System.exit(0);
                }
            }
        }
    }

    public boolean checkWinCon() {
        int flagCount = 0;
        for (int i = 0; i < buttons.length; ++i) {
            for (int j = 0; j < buttons.length; ++j) {
                if (buttons[i][j].getText().equals("F")) {
                    if (grid.isMINE(i, j)) {
                        ++flagCount;
                    } else {
                        return false;
                    }
                }
            }
        }
        if (flagCount == MineSweeper.NUM_MINES) {
            return true;
        } else {
            return false;
        }
    }
}


