package cs102.week01;

public class Main {
    public static void main(String[] args) {
        System.out.println("Hello World");
        Circle myFirstCircle = new Circle(5.8, 4.1, 7.6);
        System.out.println("My Area : " + myFirstCircle.getArea());
        myFirstCircle.r = 10.0;
        System.out.println("My Area : " + myFirstCircle.getArea());
        Circle mySecondCircle = new Circle(5.0);
        System.out.println("My Area : " + mySecondCircle.getArea());

    }
}
