package cs102.week15;

import javax.swing.*;
import java.awt.*;

public class TicTacToeFrame extends JFrame {
    public TicTacToeFrame() {

        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.setLayout(new GridLayout(3, 3));
        this.setTitle("Player x");
        this.setSize(250, 250);
        this.setResizable(false);
        this.setLocationRelativeTo(null);
        this.setVisible(true);

        ButtonListener listener = new ButtonListener(this);

        for (int i = 0; i < 9; i++) {
            JButton button = new JButton();
            button.setActionCommand("" + i);
            button.addActionListener(listener);
            this.add(button);
        }
    }
}
