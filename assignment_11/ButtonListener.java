package cs102.week15;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ButtonListener implements ActionListener {
    private Model model = new Model();
    private JFrame frame;

    public ButtonListener(JFrame frame) {
        this.frame = frame;
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        JButton button = (JButton) e.getSource();
        int buttonId = Integer.parseInt(e.getActionCommand());

        // jpg files must be in the project folder
        ImageIcon icon = new ImageIcon(model.getPlayer() + "icon.jpg");

        //this line is not important
        icon.setImage(icon.getImage().getScaledInstance(button.getWidth(), button.getHeight(), Image.SCALE_SMOOTH));

        button.setIcon(icon);
        button.setDisabledIcon(icon);

        button.setEnabled(false);

        model.updateState(buttonId);
        char winner = model.getWinner();
        if (winner == 'd') {
            JOptionPane.showMessageDialog(frame, "It's a draw");
            System.exit(0);
        } else if (winner != 0) {
            JOptionPane.showMessageDialog(frame, "Player " + winner + " wins.");
            System.exit(0);
        }

        frame.setTitle("Player " + model.getPlayer());

    }

}
