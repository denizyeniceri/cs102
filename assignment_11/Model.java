package cs102.week15;

public class Model {
    private char[][] state = {  //board state
            {'-', '-', '-'},
            {'-', '-', '-'},
            {'-', '-', '-'}
    };
    private int turnNumber = 0; // # of turns passed
    private char player = 'x'; //current player

    public char getPlayer() {
        return player;
    }


    public char getWinner() {

        /*
         * Winner is 'x', 'o', 'd', 0
         * 0 is for unfinished game, 'd' is for draw
         */

        char winner = 0;

        // check rows
        for (int i = 0; i < 3; ++i) {
            if (state[i][0] != '-' &&
                    state[i][0] == state[i][1] &&
                    state[i][0] == state[i][2]) {
                winner = state[i][0];
                return winner;
            }
        }

        // check columns
        for (int i = 0; i < 3; ++i) {
            if (state[0][i] != '-' &&
                    state[0][i] == state[1][i] &&
                    state[0][i] == state[2][i]) {
                winner = state[0][i];
                return winner;
            }
        }

        // left diagonal
        if (state[0][0] != '-' &&
                state[0][0] == state[1][1] &&
                state[0][0] == state[2][2]) {
            winner = state[0][0];
            return winner;
        }

        // right diagonal
        if (state[0][2] != '-' &&
                state[0][2] == state[1][1] &&
                state[0][2] == state[2][0]) {
            winner = state[0][2];
            return winner;
        }

        //check draw
        if (turnNumber == 9)
            winner = 'd';


        return winner;
    }

    public void updateState(int buttonId) {
        state[buttonId / 3][buttonId % 3] = player;
        turnNumber++;

        if (player == 'x')
            player = 'o';
        else if (player == 'o')
            player = 'x';
    }
}
