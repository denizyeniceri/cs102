package cs102.week07;

public class Food extends Product {
    private double kgs;
    private String expirationDate;

    public Food(double taxRate, double kgs) {
        super(taxRate);
        this.kgs = kgs;
        this.expirationDate = "2020-11-13";
    }

    public Food(String name) {
        super();
        this.expirationDate = "2020-11-13";
        this.setName(name);
    }

    public double getKgs() {
        return this.kgs;
    }

    public void setKgs(double kgs) {
        this.kgs = kgs;
    }

    public String getExpirationDate() {
        return this.expirationDate;
    }

    public void setExpirationDate(String expirationDate) {
        this.expirationDate = expirationDate;
    }

    public String toString() {
        return super.toString() +
                "Weight: " + this.kgs + "\n" +
                "Expiration date: " + this.expirationDate + "\n";
    }

    public double calculateTotalPrice() {
        return (getPrice() * (getTaxRate() + 100) / 100) * this.kgs;
    }
}
