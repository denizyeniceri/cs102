package cs102.week05;

public class Main {
    public static void main(String[] args) {
        Skirt skirt = new Skirt(1, "A", "red", 15, "the skirt");
        skirt.setTaxRate(18);
        skirt.setName("skirt");
        skirt.setQuantity(2);
        System.out.println(skirt);

        Trousers jean = new Trousers(30, "blue", "B");
        jean.setPrice(50.99);
        jean.setName("skinny jean");
        jean.setQuantity(1);
        System.out.println(jean);

        Dairy milk = new Dairy(25, 1, true, "milk");
        milk.setExpirationDate("2021-03-09");
        milk.setPrice(2.95);
        System.out.println(milk);

        Vegetable pepper = new Vegetable(18, 0.5, false, "pepper");
        System.out.println(pepper);

        DishWashing tablet = new DishWashing("C");
        tablet.setLts(30);
        tablet.setLiquid(false);
        tablet.setName("tablet");
        System.out.println(tablet);
    }
}
