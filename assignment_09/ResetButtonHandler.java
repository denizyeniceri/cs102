package cs102.week11;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ResetButtonHandler implements ActionListener {
    private JRadioButton radioButton;
    private JTextField textField;
    private JLabel label;

    public ResetButtonHandler(JRadioButton radioButton, JTextField textField, JLabel label) {
        this.radioButton = radioButton;
        this.textField = textField;
        this.label = label;
    }

    public void actionPerformed(ActionEvent actionEvent) {
        this.label.setText("Hi.");
        this.textField.setText("");
        this.radioButton.setSelected(true);
    }
}
