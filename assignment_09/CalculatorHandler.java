package cs102.week11;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class CalculatorHandler implements ActionListener {

    private JLabel label;

    public CalculatorHandler(JLabel label) {
        this.label = label;
    }

    public void actionPerformed(ActionEvent actionEvent) {
        if (actionEvent.getSource() instanceof JButton) {
            JButton button = (JButton) actionEvent.getSource();

            if (button.getText().equals("=")) {
                if (label.getText().contains("+")) {
                    String arguments[] = label.getText().trim().split("\\+");
                    double resultOfSummation = 0;
                    for (int i = 0; i < arguments.length; ++i) {
                        double currentNumber = Double.parseDouble(arguments[i]);
                        resultOfSummation += currentNumber;
                    }
                    label.setText("" + resultOfSummation);
                } else if (label.getText().contains("x")) {
                    String arguments[] = label.getText().trim().split("x");
                    double resultOfMultiplication = 1;
                    for (int i = 0; i < arguments.length; ++i) {
                        double currentNumber = Double.parseDouble(arguments[i]);
                        resultOfMultiplication *= currentNumber;
                    }
                    label.setText("" + resultOfMultiplication);
                } else if (label.getText().contains("-")) {
                    String arguments[] = label.getText().trim().split("-");
                    double resultOfSubtraction = 0;
                    if (arguments.length > 1) {
                        resultOfSubtraction = Double.parseDouble(arguments[0]);
                        for (int i = 1; i < arguments.length; ++i) {
                            double currentNumber = Double.parseDouble(arguments[i]);
                            resultOfSubtraction -= currentNumber;
                        }
                    }
                    label.setText("" + resultOfSubtraction);
                } else if (label.getText().contains("/")) {
                    String arguments[] = label.getText().trim().split("/");
                    double resultOfDivision = 0;
                    for (int i = 0; i < arguments.length; ++i) {
                        double currentNumber = Double.parseDouble(arguments[i]);
                        if (i == 0)
                            resultOfDivision = currentNumber;
                        else
                            resultOfDivision = resultOfDivision / currentNumber;
                    }
                    label.setText("" + resultOfDivision);
                }
            } else if (button.getText().equals("C")) {
                label.setText(" ");
            } else {
                label.setText(label.getText() + button.getText());
            }
        }
    }
}
