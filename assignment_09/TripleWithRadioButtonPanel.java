package cs102.week11;

import javax.swing.*;

public class TripleWithRadioButtonPanel extends JPanel {
    private JTextField textField;
    private JButton appendButton, resetButton;
    private JLabel label;
    private JRadioButton radioButtonForLowercase;
    private JRadioButton radioButtonForUppercase;

    public TripleWithRadioButtonPanel() {
        textField = new JTextField(20);
        appendButton = new JButton("Append");
        resetButton = new JButton("Reset");
        label = new JLabel("Hi.");
        radioButtonForLowercase = new JRadioButton("lowercase", true);
        radioButtonForUppercase = new JRadioButton("UPPERCASE", false);

        add(label);
        add(textField);
        add(appendButton);
        add(resetButton);
        add(radioButtonForLowercase);
        add(radioButtonForUppercase);

        ButtonGroup group = new ButtonGroup();
        group.add(radioButtonForLowercase);
        group.add(radioButtonForUppercase);

        this.appendButton.addActionListener(new AppendButtonHandler(
                this.radioButtonForLowercase, this.textField, this.label));
        this.textField.addActionListener(new AppendButtonHandler(
                this.radioButtonForLowercase, this.textField, this.label));
        this.resetButton.addActionListener(new ResetButtonHandler(
                this.radioButtonForLowercase, this.textField, this.label));
    }
}
