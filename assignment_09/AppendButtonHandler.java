package cs102.week11;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class AppendButtonHandler implements ActionListener {
    private JRadioButton radioButton;
    private JTextField textField;
    private JLabel label;

    public AppendButtonHandler(JRadioButton radioButton, JTextField textField, JLabel label) {
        this.radioButton = radioButton;
        this.textField = textField;
        this.label = label;
    }

    public void actionPerformed(ActionEvent actionEvent) {
        String stringToAppend = "";
        if (this.radioButton.isSelected()) {
            stringToAppend = this.textField.getText().toLowerCase();
        } else {
            stringToAppend = this.textField.getText().toUpperCase();
        }
        this.label.setText(this.label.getText() + stringToAppend);
        this.textField.setText("");
    }
}
