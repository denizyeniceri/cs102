package cs102.week08;

import java.util.ArrayList;

public class Subway {
    private ArrayList<City> cities;

    public Subway() {
        this.cities = new ArrayList<City>();
    }

    public void addCity(City city) {
        this.cities.add(city);
    }

    public String toString() {
        String connectionInfo = "Subway connectivity info: \n" ;
        for (City city : this.cities) {
            connectionInfo += city;
        }

        return connectionInfo;
    }
}
