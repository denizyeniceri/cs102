package cs102.week08;

import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
        Subway subway = new Subway();

        City A = new City("A");
        City B = new City("B");
        City C = new City("C");
        City D = new City("D");
        City E = new City("E");
        City F = new City("F");

        Route AB = new Route(A, B, 10);
        Route AC = new Route(A, C, 25);
        Route AD = new Route(A, D, 20);
        Route AF = new Route(A, F, 8);
        Route BD = new Route(B, D, 14);
        Route BE = new Route(B, E, 10);
        Route CD = new Route(C, D, 5);
        Route DE = new Route(D, E, 40);
        Route EF = new Route(E, F, 12);

        A.addRoute(AB);
        A.addRoute(AC);
        A.addRoute(AD);
        A.addRoute(AF);
        B.addRoute(BD);
        B.addRoute(BE);
        C.addRoute(CD);
        D.addRoute(DE);
        E.addRoute(EF);

        subway.addCity(A);
        subway.addCity(B);
        subway.addCity(C);
        subway.addCity(D);
        subway.addCity(E);
        subway.addCity(F);

        findShortestPath(A, D);
    }

    public static void findShortestPath(City source, City destination) {
        ArrayList<Route> routesFromSource = source.getConnections();
        ArrayList<Route> totalRoutes = new ArrayList<Route>();

        for (Route eachRoute : routesFromSource) {
            ArrayList<Route> routes = eachRoute.getDestination().getConnections();
            for (Route route : routes) {
                totalRoutes.add(new Route(source, route.getDestination(),
                        eachRoute.getDistance() + route.getDistance()));
            }
        }

        ArrayList<Route> routesFromSourceToDest = new ArrayList<Route>();
        collectRoutesFromSourceToDest(totalRoutes, source, destination,
                routesFromSourceToDest);
        collectRoutesFromSourceToDest(routesFromSource, source, destination,
                routesFromSourceToDest);
        double min = Double.MAX_VALUE;
        Route routeMin = null;
        for (Route route : routesFromSourceToDest) {
            if (route.getDistance() < min) {
                min = route.getDistance();
                routeMin = route;
            }
        }
        System.out.println(routeMin);
    }

    public static void collectRoutesFromSourceToDest(ArrayList<Route>
                                                             routesDetected, City source, City destination, ArrayList<Route>
                                                             routesFromSourceToDest) {
        for (Route route : routesDetected) {
            if (route.getSource().getName().equals(source.getName())
                    && route.getDestination().getName().equals(destination.getName()))
                routesFromSourceToDest.add(route);
        }
    }
}
