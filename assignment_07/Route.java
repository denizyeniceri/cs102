package cs102.week08;

public class Route {
    private City source;
    private City destination;
    private double distance;

    public Route(City source, City destination, double distance) {
        this.source = source;
        this.destination = destination;
        this.distance = distance;
    }

    public City getSource() {
        return this.source;
    }

    public City getDestination() {
        return this.destination;
    }

    public double getDistance() {
        return this.distance;
    }

    public String toString() {
        return "From: " + this.source.getName() + "\n" +
                "To: " + this.destination.getName() + "\n" +
                "Distance: " + this.distance + "\n";
    }
}
