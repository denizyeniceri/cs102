package cs102.week08;

import java.util.ArrayList;

public class City {
    private String name;
    private ArrayList<Route> connections;

    public City(String name) {
        this.name = name;
        this.connections = new ArrayList<Route>();
    }

    public void addRoute(Route route) {
        this.connections.add(route);
    }

    public String getName() {
        return this.name;
    }

    public ArrayList<Route> getConnections() {
        return this.connections;
    }

    public String toString() {
        String connectionInfo = this.name + ":\n";
        for (Route route: this.connections) {
            connectionInfo += route;
        }

        return connectionInfo;
    }
}
