package cs102.week04;

public class Book {
    private String title;
    private String isbn;
    private int numberOfPages;
    private boolean onLoan;

    public Book(String title, int numberOfPages, String isbn) {
        this.title = title;
        this.isbn = isbn;
        this.numberOfPages = numberOfPages;
        this.onLoan = false;
    }

    public String toString() {
        return "Title: " + this.title + "\n"
                + "ISBN: " + this.isbn + "\n"
                + "Number of Pages: " + this.numberOfPages + "\n"
                + "Loan Status: " + this.onLoan + "\n";
    }

    public String getTitle() {
        return this.title;
    }

    public String getISBN() {
        return this.isbn;
    }

    public int getNumberOfPages() {
        return this.numberOfPages;
    }

    public boolean isOnLoan() {
        return this.onLoan;
    }

    public void setOnLoan(boolean onLoan) {
        this.onLoan = onLoan;
    }
}
